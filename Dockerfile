FROM python:3.9
WORKDIR /app
COPY Pipfile Pipfile.lock /app/

RUN pip install pipenv \
 && pipenv install --system

COPY logging.conf src /app/
CMD ["python", "bot.py"]
