from player import (
    Blue,
    Merlin,
    Perceval,
    Red,
    Morgana,
    Oberon,
    Mordred,
)

ROLES = [
    Blue(),
    Blue(),
    # Blue(),
    Merlin(),
    Perceval(),
    Red(),
    Morgana(),
    # Mordred(),
    Oberon(),
]

_QUEST_NUM_MAP = {
    5: {
        1: 2,
        2: 3,
        3: 2,
        4: 3,
        5: 3,
    },
    6: {
        1: 2,
        2: 3,
        3: 4,
        4: 3,
        5: 4,
    },
    7: {
        1: 2,
        2: 3,
        3: 3,
        4: 4,
        5: 4,
    },
    8: {
        1: 3,
        2: 4,
        3: 4,
        4: 5,
        5: 5,
    },
    9: {
        1: 3,
        2: 4,
        3: 4,
        4: 5,
        5: 5,
    },
    10: {
        1: 3,
        2: 4,
        3: 4,
        4: 5,
        5: 5,
    },
}

QUEST_NUM_MAP = _QUEST_NUM_MAP[len(ROLES)]
