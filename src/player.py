class Player:

    def __init__(self, member, role):
        self.member = member
        self.role = role


class Role:

    id = None
    color = None
    name = None

    @classmethod
    def get_by_id(cls, id):
        id_map = {
            0: Blue,
            1: Merlin,
            2: Perceval,
            3: Red,
            4: Morgana,
            5: Oberon,
            6: Mordred,
        }
        return id_map[id]()

    def initial_message(self, players):
        message = f'あなたは「{self.name}」です'
        special_message = self.special_message(players)
        if special_message:
            return f'{message}\n{special_message}'
        return message

    def special_message(self, players):
        return


class Blue(Role):

    id = 0
    color = 'blue'
    name = 'アーサーの下僕'


class Merlin(Blue):

    id = 1
    name = 'マーリン'

    def special_message(self, players):
        name = ' '.join([p.member.name for p in players
                            if p.role.color == 'red' and p.role.id != 6])
        return f'邪悪の陣営は {name} です'


class Perceval(Blue):

    id = 2
    name = 'パーシヴァル'

    def special_message(self, players):
        name = ' か '.join([p.member.name for p in players
                               if p.role.id in (1, 4)])
        return f'マーリンは {name} です'


class Red(Role):

    id = 3
    color = 'red'
    name = 'モードレッドの手下'

    def special_message(self, players):
        name = ' '.join([p.member.name for p in players
                            if p.role.color == 'red' and p.role.id != 5])
        return f'邪悪の陣営は {name} です'


class Morgana(Red):

    id = 4
    name = 'モルガナ'


class Oberon(Red):

    id = 5
    name = 'オベロン'

    def special_message(self, players):
        return


class Mordred(Red):

    id = 6
    name = 'モードレッド'
