import os
import discord
import redis
import time
import logging
import logging.config
from discord.ext import commands

intents = discord.Intents.default()
intents.members = True
bot = commands.Bot(command_prefix='.', intents=intents)

# local
redis_cli = redis.StrictRedis(host='127.0.0.1', port=6379)

# docker-compose
# redis_cli = redis.StrictRedis(host='redis', port=6379)

logging.config.fileConfig('logging.conf')
logger = logging.getLogger('Bot')


def load_game():
    from game import Game
    from player import Player, Role

    player_num = redis_cli.get('player_num')
    if player_num is None:
        return Game()

    player_num = int(player_num)
    channel = bot.get_channel(int(redis_cli.get('channel_id')))
    quest_idx = int(redis_cli.get('quest_idx'))
    player_idx = int(redis_cli.get('player_idx'))
    success_count = int(redis_cli.get('success_count'))
    failure_count = int(redis_cli.get('failure_count'))
    reject_count = int(redis_cli.get('reject_count'))

    players = []
    for idx in range(player_num):
        member = bot.get_user(int(redis_cli.get(f'player_id:{idx}')))
        role = Role.get_by_id(int(redis_cli.get(f'role:{idx}')))
        players.append(Player(member, role))

    return Game.load(channel, players, quest_idx, player_idx,
                     success_count, failure_count, reject_count)


def save_game():
    redis_cli.set('channel_id', game.channel.id)
    redis_cli.set('quest_idx', game.quest_idx)
    redis_cli.set('player_idx', game.player_idx)
    redis_cli.set('player_num', len(game.players))
    redis_cli.set('success_count', game.success_count)
    redis_cli.set('failure_count', game.failure_count)
    redis_cli.set('reject_count', game.quest.reject_count)
    for idx, player in enumerate(game.players):
        redis_cli.set(f'player_id:{idx}', player.member.id)
        redis_cli.set(f'role:{idx}', player.role.id)
    logger.info(f'save game: {game}')


@bot.event
async def on_ready():
    logger.info('start')
    global game
    game = load_game()


@bot.command()
async def resistance(ctx, *members: discord.Member):
    if type(ctx.channel) == discord.DMChannel:
        await ctx.send('`.resistance` はチャネルに送信してね')
        return
    game.start(list(members), ctx.channel)
    await game.play()
    await game.quest.play()
    save_game()


@bot.command()
async def restart(ctx):
    if type(ctx.channel) == discord.DMChannel:
        await ctx.send('`.restart` はチャネルに送信してね')
        return
    game.start([p.member for p in game.players], ctx.channel)
    await game.play()
    await game.quest.play()
    save_game()


@bot.command()
async def quest(ctx, *members: discord.Member):
    if type(ctx.channel) == discord.DMChannel:
        await ctx.send('`.quest` はチャネルに送信してね')
        return

    if ctx.channel != game.channel:
        await ctx.send(f'{ctx.author.mention} 送信先のチャネルが違うよ')
        return

    await game.quest.resister(members)


@bot.command()
async def accept(ctx):
    if (type(ctx.channel) != discord.DMChannel or bot.user != ctx.channel.me):
        await ctx.send(f'{ctx.author.mention} `.accept` は DM に送信してね')
        return
    await ctx.send('「賛成」を受け取りました')
    await game.channel.send(f'{ctx.author.mention} が投票しました')
    logger.info(f'voted: {ctx.author.name}')
    await game.quest.vote(ctx.author, True)
    save_game()


@bot.command()
async def reject(ctx):
    if (type(ctx.channel) != discord.DMChannel or bot.user != ctx.channel.me):
        await ctx.send(f'{ctx.author.mention} `.reject` は DM に送信してね')
        return
    await ctx.send('「反対」を受け取りました')
    await game.channel.send(f'{ctx.author.mention} が投票しました')
    logger.info(f'voted: {ctx.author.name}')
    await game.quest.vote(ctx.author, False)
    save_game()


@bot.command()
async def success(ctx):
    if (type(ctx.channel) != discord.DMChannel or bot.user != ctx.channel.me):
        await ctx.send(f'{ctx.author.mention} `.success` は DM に送信してね')
        return
    await ctx.send('「成功」を受け取りました')
    await game.channel.send(f'{ctx.author.mention} がクエストを実行しました')
    logger.info(f'executed: {ctx.author.name}')
    await game.quest.recieve(ctx.author, True)
    save_game()


@bot.command()
async def failure(ctx):
    if (type(ctx.channel) != discord.DMChannel or bot.user != ctx.channel.me):
        await ctx.send(f'{ctx.author.mention} `.fail` は DM に送信してね')
        return
    await ctx.send('「失敗」を受け取りました')
    await game.channel.send(f'{ctx.author.mention} がクエストを実行しました')
    logger.info(f'executed: {ctx.author.name}')
    await game.quest.recieve(ctx.author, False)
    save_game()


@bot.command()
async def merlin(ctx, member: discord.Member):
    if type(ctx.channel) == discord.DMChannel:
        await ctx.send('`.merlin` はチャネルに送信してね')
        return

    if ctx.channel != game.channel:
        await ctx.send(f'{ctx.author.mention} 送信先のチャネルが違うよ')
        return

    if game.success_count < 3:
        await ctx.send(f'{ctx.author.mention} マーリンの暗殺フェーズじゃないよ')
        return

    await game.assassinate_merlin(member)
    save_game()


@bot.command()
async def over(ctx):
    import random
    if type(ctx.channel) == discord.DMChannel:
        await ctx.send('`.over` はチャネルに送信してね')
        return

    if ctx.channel != game.channel:
        await ctx.send(f'{ctx.author.mention} 送信先のチャネルが違うよ')
        return

    if random.random() < 0.5:
        await game.end(True)
    else:
        await game.end(False)

    save_game()


@bot.command()
async def wordwolf(ctx, *members: discord.Member):
    import random
    if type(ctx.channel) == discord.DMChannel:
        await ctx.send('`.wordwolf` はチャネルに送信してね')
        return

    members = list(members)
    random.shuffle(members)
    leader = members[0]
    await ctx.send(f'リーダーは {leader.mention} だよ')
    await members[0].send(f'おおかみは {members[1].name} 、'
                          f'きつねは {members[2].name} です\n'
                          'いい感じのお題を送ってね :wink:')

    members = [m for m in members if m != leader]
    random.shuffle(members)
    participant = ' :arrow_right: '.join([m.mention for m in members])
    await ctx.send(f'順番： {participant}')


bot.run(os.environ['DISCORD_TOKEN'])
