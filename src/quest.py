import logging
import discord
from const import QUEST_NUM_MAP

logger = logging.getLogger('Quest')


class Quest:

    def __init__(self, game, reject_count=0):
        self.game = game
        self.idx = self.game.quest_idx
        self.reject_count = reject_count
        self.voted = {}
        self.members = []
        self.recieved = {}

    async def play(self):
        leader = self.game.current_player.member.mention
        quest_num = QUEST_NUM_MAP.get(self.idx, 1)
        embed = discord.Embed(
            title=f'クエスト {self.idx}',
            description=(f'クエスト {self.idx} の参加者を '
                         f'{quest_num} 人選んでください。'),
            color=discord.Colour.gold())
        embed.add_field(name='リーダー', value=leader, inline=False)
        embed.add_field(name='棄却回数', value=self.reject_count, inline=False)
        embed.add_field(name='選び方',
                        value='```.quest @Soup @Soup ...```', inline=False)
        await self.game.channel.send(embed=embed)

    async def resister(self, members):
        self.members = members
        participant = ' '.join([m.mention for m in members])
        embed = discord.Embed(
            title=f'クエスト {self.idx}',
            description=(f'クエスト {self.idx} の参加者について、'
                         '賛成の人は `.accept` 、反対の人は `.reject` を '
                         'DM に送信してください。'),
            color=discord.Colour.green())
        embed.add_field(name='参加者', value=participant, inline=False)
        embed.add_field(name='棄却回数', value=self.reject_count, inline=False)
        await self.game.channel.send(embed=embed)

        for p in self.game.players:
            await p.member.send(embed=embed)

        names = ', '.join([m.name for m in members])
        logger.info(f'registered: idx={self.idx}, '
                    f'reject_count={self.reject_count}, '
                    f'members=[{names}]')

    async def vote(self, member, result):
        self.voted[member.name] = result

        results = []
        for p in self.game.players:
            if p.member.name not in self.voted:
                return
            results.append(self.voted[p.member.name])

        accept_count = len([r for r in results if r is True])
        reject_count = len([r for r in results if r is False])

        if accept_count > reject_count:
            await self.accept()
        else:
            await self.reject()

    async def accept(self):
        self.game.forward_player()
        participant = ' '.join([m.mention for m in self.members])

        accepted = []
        rejected = []
        for p in self.game.players:
            if self.voted[p.member.name]:
                accepted.append(p.member.mention)
            else:
                rejected.append(p.member.mention)

        embed = discord.Embed(
            title=f'クエスト {self.idx}',
            description=(f'クエスト {self.idx} の参加者が承認されました。\n'
                         '参加者は `.success` または `.failure` を '
                         'DM に送信してください。'),
            color=discord.Colour.orange())
        if accepted:
            embed.add_field(name='賛成', value=' '.join(accepted), inline=False)
        if rejected:
            embed.add_field(name='反対', value=' '.join(rejected), inline=False)
        embed.add_field(name='参加者', value=participant, inline=False)
        await self.game.channel.send(embed=embed)
        for m in self.members:
            await m.send(embed=embed)

        names = ', '.join([m.name for m in self.members])
        logger.info(f'accepted: idx={self.idx}, '
                    f'reject_count={self.reject_count}, '
                    f'members=[{names}]')

    async def reject(self):
        self.reject_count += 1

        if self.reject_count >= 5:
            await self.game.end(False)
            return

        self.game.forward_player()
        leader = self.game.current_player.member.mention

        accepted = []
        rejected = []
        for p in self.game.players:
            if self.voted[p.member.name]:
                accepted.append(p.member.mention)
            else:
                rejected.append(p.member.mention)

        self.voted = {}

        quest_num = QUEST_NUM_MAP.get(self.idx, 1)
        embed = discord.Embed(
            title=f'クエスト {self.idx}',
            description=(f'クエスト {self.idx} の参加者は棄却されました。\n'
                         f'クエスト {self.idx} の参加者を '
                         f'{quest_num} 人選んでください。'),
            color=discord.Colour.purple())
        embed.add_field(name='リーダー', value=leader, inline=True)
        embed.add_field(name='棄却回数', value=self.reject_count, inline=True)
        if accepted:
            embed.add_field(name='賛成', value=' '.join(accepted), inline=False)
        if rejected:
            embed.add_field(name='反対', value=' '.join(rejected), inline=False)
        embed.add_field(name='選び方',
                        value='```.quest @Soup @Soup ...```', inline=False)
        await self.game.channel.send(embed=embed)
        logger.info(f'rejected: idx={self.idx}, '
                    f'reject_count={self.reject_count}')

    async def recieve(self, member, result):
        self.recieved[member.name] = result

        results = []
        for m in self.members:
            if m.name not in self.recieved:
                return
            results.append(self.recieved[m.name])

        success_count = len([r for r in results if r is True])
        failure_count = len([r for r in results if r is False])

        names = ', '.join([m.name for m in self.members])
        if (failure_count == 0
                or (len(self.game.players) >= 7
                        and failure_count == 1 and self.idx == 4)):
            msg = '成功'
            color = discord.Colour.blue()
            succeeded = True
            logger.info(f'succeeded: idx={self.idx}, member=[{names}]')
        else:
            msg = '失敗'
            color = discord.Colour.red()
            succeeded = False
            logger.info(f'failed: idx={self.idx}, member=[{names}]')

        participant = ' '.join([m.mention for m in self.members])
        embed = discord.Embed(
            title=f'クエスト {self.idx}',
            description=f'クエスト {self.idx} は {msg} です',
            color=color)
        embed.add_field(name='参加者', value=participant, inline=False)
        embed.add_field(name='成功', value=success_count, inline=True)
        embed.add_field(name='失敗', value=failure_count, inline=True)
        await self.game.channel.send(embed=embed)

        await self.game.forward_quest(succeeded)
