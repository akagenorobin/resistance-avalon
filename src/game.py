import logging
import random
import discord
from quest import Quest
from player import Player
from const import ROLES

logger = logging.getLogger('Game')


class Game:

    def __str__(self):
        return (f'<Game channel={self.channel.name} '
                f'quest_idx={self.quest_idx} player_idx={self.player_idx} '
                f'success_count={self.success_count} '
                f'failure_count={self.failure_count} '
                f'reject_count={self.quest.reject_count}>')

    def init(self):
        self.quest_idx = 1
        self.success_count = 0
        self.failure_count = 0

    @classmethod
    def load(cls, channel, players, quest_idx, player_idx,
             success_count, failure_count, reject_count):
        game = cls()
        game.channel = channel
        game.players = players
        game.quest_idx = quest_idx
        game.player_idx = player_idx
        game.success_count = success_count
        game.failure_count = failure_count
        game.quest = Quest(game, reject_count=reject_count)
        logger.info(f'load: {game}')
        players = ', '.join([f'{p.member.name}={p.role.name}'
                             for p in game.players])
        logger.debug(f'role: {players}')
        return game

    @property
    def current_player(self):
        return self.players[self.player_idx]

    def forward_player(self):
        self.player_idx = (self.player_idx + 1) % len(self.players)

    def start(self, members, channel):
        random.shuffle(members)
        roles = ROLES.copy()
        random.shuffle(roles)

        self.players = [Player(m, r) for m, r in zip(members, roles)]
        self.channel = channel
        self.quest_idx = 1
        self.player_idx = 0
        self.success_count = 0
        self.failure_count = 0
        self.quest = Quest(self)
        logger.info(f'start: {self}')
        players = ', '.join([f'{p.member.name}={p.role.name}'
                             for p in self.players])
        logger.debug(f'role: {players}')

    async def play(self):
        embed = discord.Embed(
            title='レジスタンス・アヴァロン',
            description='レジスタンス・アヴァロンを始めるよ！',
            color=discord.Colour.dark_magenta())
        participant = ' :arrow_right: '.join(
            [p.member.mention for p in self.players])
        embed.add_field(name='参加者', value=participant, inline=False)
        role_emoji = {
            'blue': ':blue_circle:',
            'red': ':red_circle:',
        }
        role_message = '、 '.join(
            [f'{role_emoji[role.color]} {role.name}' for role in ROLES])
        embed.add_field(name='役職', value=role_message, inline=False)
        await self.channel.send(embed=embed)

        for p in self.players:
            await p.member.send(p.role.initial_message(self.players))

    async def forward_quest(self, succeeded):
        if succeeded:
            self.success_count += 1
        else:
            self.failure_count += 1

        if self.success_count >= 3:
            await self.ask_merlin()
        elif self.failure_count >= 3:
            await self.end(False)
        else:
            embed = discord.Embed(title=f'現在のクエスト状況',
                                  color=discord.Colour.dark_blue())
            embed.add_field(name='成功', value=self.success_count, inline=True)
            embed.add_field(name='失敗', value=self.failure_count, inline=True)
            await self.channel.send(embed=embed)

            self.quest_idx += 1
            self.quest = Quest(self)
            await self.quest.play()

            logger.info(f'forward_quest: {self}')

    async def ask_merlin(self):
        red = []
        for p in self.players:
            if p.role.color == 'red':
                red.append(p)

        embed = discord.Embed(
            title='マーリンの暗殺',
            description=('正義の陣営が3勝しました。\n'
                         '邪悪の陣営はマーリンを指名してください。'),
            color=discord.Colour.dark_gray())
        mention = ' '.join([p.member.mention for p in red])
        embed.add_field(name='邪悪の陣営', value=mention, inline=False)
        embed.add_field(name='選び方', value='```.merlin @Soup```', inline=False)
        await self.channel.send(embed=embed)

        names = ', '.join([p.member.name for p in red])
        logger.info(f'ask merlin: red=[{names}]')

    async def assassinate_merlin(self, target):
        merlin = None
        for p in self.players:
            if p.role.id == 1:
                merlin = p

        logger.info(f'assassinate merlin: merlin={merlin.member.name}, '
                    f'target={target.name}')

        if merlin.member.name == target.name:
            await self.end(False)
        else:
            await self.end(True)

    async def end(self, blue_wins):
        if blue_wins:
            winner = '正義'
            color = discord.Colour.blue()
            logger.info(f'end: Blue wins')
        else:
            winner = '邪悪'
            color = discord.Colour.red()
            logger.info(f'end: Red wins')
        embed = discord.Embed(
            title='レジスタンス・アヴァロン',
            description=f'{winner}の陣営の勝利です！\nおめでとうございます :tada:',
            color=color)

        role_emoji = {
            'blue': ':blue_circle:',
            'red': ':red_circle:',
        }
        role_message = '、 '.join(
            [f'{role_emoji[role.color]} {role.name}' for role in ROLES])

        for p in sorted(self.players, key=lambda x: x.role.id):
            embed.add_field(name=f'{role_emoji[p.role.color]} {p.role.name}',
                            value=p.member.mention, inline=True)

        await self.channel.send(embed=embed)
        self.init()
